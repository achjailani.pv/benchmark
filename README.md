# Benchmark

### Read benchmark
```shell
goos: darwin
goarch: arm64
pkg: foo/benchmark/strs
BenchmarkConcatenateBuffer-8    31290266                37.88 ns/op           72 B/op          2 allocs/op
PASS
ok      foo/benchmark/strs      1.377s
```
* `goos`, `goarch`, `pkg` describe the operating system, architecture and package
* `BenchmarkConcatenateBuffer` the name of benchmark function
* The `-8` suffix is suffix denotes the number of CPUs used to run the benchmark, as specified by `GOMAXPROCS`
* The Second column: Number of iterations, indicated by `b.N` inside function
* The Third column: Nanoseconds per operation
* The Fourth column: Number of bytes allocated per operation
* The Fifth column: Number of allocations per operation



### Commands
```shell
go test -bench . 
```

Complete information
```shell
go test -bench . -benchmem
```

With count
```shell
go test -bench . -benchmem -count 5
```

Ex:

```shell
go test -bench BenchmarkConcatenateBuffer -benchmem -count 5
goos: darwin
goarch: arm64
pkg: foo/benchmark/strs
BenchmarkConcatenateBuffer-8    31155073                37.88 ns/op           72 B/op          2 allocs/op
BenchmarkConcatenateBuffer-8    31343834                37.77 ns/op           72 B/op          2 allocs/op
BenchmarkConcatenateBuffer-8    31797274                37.90 ns/op           72 B/op          2 allocs/op
BenchmarkConcatenateBuffer-8    31257427                37.83 ns/op           72 B/op          2 allocs/op
BenchmarkConcatenateBuffer-8    32955602                38.07 ns/op           72 B/op          2 allocs/op
PASS
ok      foo/benchmark/strs      6.659s

```

With `benchtime`
```shell
go test -bench BenchmarkConcatenateBuffer -benchmem -count 5 -benchtime=100x
```



### Reference
* [logrocket](https://blog.logrocket.com/benchmarking-golang-improve-function-performance/)
* [practical-go-lesson](https://www.practical-go-lessons.com)