package strs

import (
	"bytes"
	"strings"
)

// ConcatenateBuffer is a function
func ConcatenateBuffer(first string, second string) string {
	var buffer bytes.Buffer
	buffer.WriteString(first)
	buffer.WriteString(second)
	return buffer.String()
}

// ConcatenateJoin is a function
func ConcatenateJoin(first string, second string) string {
	return strings.Join([]string{first, second}, "")
}
