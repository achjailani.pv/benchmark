package strs_test

import (
	"foo/benchmark/strs"
	"testing"
)

func BenchmarkConcatenateBuffer(b *testing.B) {
	var val string
	for i := 0; i < b.N; i++ {
		val = strs.ConcatenateBuffer("val1", "val2")
	}

	_ = val
}

func BenchmarkConcatenateJoin(b *testing.B) {
	var val string
	for i := 0; i < b.N; i++ {
		val = strs.ConcatenateJoin("val1", "val2")
	}

	_ = val
}
