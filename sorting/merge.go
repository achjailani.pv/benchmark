package sorting

import (
	"strconv"
	"strings"
)

// Merge is a struct
type Merge struct {
	data []int
}

// NewMerge is a constructor
func NewMerge(data []int) *Merge {
	return &Merge{data: data}
}

// Sort is a public method
func (mg *Merge) Sort() {
	min := 0
	max := len(mg.data) - 1

	mg.sort(mg.data, min, max)
}

// sort is a private method
func (mg *Merge) sort(arr []int, l int, r int) {
	if l < r {
		m := l + (r-l)/2

		mg.sort(arr, l, m)
		mg.sort(arr, m+1, r)

		mg.merge(arr, l, m, r)
	}
}

// merge is a private method
func (mg *Merge) merge(arr []int, l int, m int, r int) {
	n1 := m - l + 1
	n2 := r - m

	L := make([]int, n1)
	R := make([]int, n2)

	for i := 0; i < n1; i++ {
		L[i] = arr[l+i]
	}
	for j := 0; j < n2; j++ {
		R[j] = arr[m+1+j]
	}

	i := 0
	j := 0
	k := l

	for i < n1 && j < n2 {
		if L[i] <= R[j] {
			arr[k] = L[i]
			i++
		} else {
			arr[k] = R[j]
			j++
		}
		k++
	}

	for i < n1 {
		arr[k] = L[i]
		i++
		k++
	}

	for j < n2 {
		arr[k] = R[j]
		j++
		k++
	}
}

// ToString is method to convert string
func (mg *Merge) ToString() string {
	var s []string
	for _, r := range mg.data {
		s = append(s, strconv.Itoa(r))
	}

	return strings.Join(s, " ")
}

func MergeSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}

	mid := len(arr) / 2
	left := MergeSort(arr[:mid])
	right := MergeSort(arr[mid:])

	return merge(left, right)
}

func merge(left, right []int) []int {
	result := make([]int, 0)
	leftIndex, rightIndex := 0, 0

	for leftIndex < len(left) && rightIndex < len(right) {
		if left[leftIndex] < right[rightIndex] {
			result = append(result, left[leftIndex])
			leftIndex++
		} else {
			result = append(result, right[rightIndex])
			rightIndex++
		}
	}

	result = append(result, left[leftIndex:]...)
	result = append(result, right[rightIndex:]...)

	return result
}
