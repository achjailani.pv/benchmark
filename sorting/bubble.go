package sorting

import (
	"strconv"
	"strings"
)

// Bubble is struct hold data
type Bubble struct {
	data []int
}

// NewBubble is a constructor
func NewBubble(data []int) *Bubble {
	return &Bubble{data: data}
}

// SortASC is a method to short
func (b *Bubble) SortASC() {
	for i := 0; i < len(b.data); i++ {
		for j := 0; j < len(b.data)-1; j++ {
			if b.data[j] > b.data[j+1] {
				b.data[j], b.data[j+1] = b.data[j+1], b.data[j]
			}
		}
	}
}

// SortDESC is method to reverse
func (b *Bubble) SortDESC() {
	for i := 0; i < len(b.data); i++ {
		for j := 0; j < len(b.data)-1; j++ {
			if b.data[j] < b.data[j+1] {
				b.data[j+1], b.data[j] = b.data[j], b.data[j+1]
			}
		}
	}
}

// ToString is method to convert string
func (b *Bubble) ToString() string {
	var s []string
	for _, r := range b.data {
		s = append(s, strconv.Itoa(r))
	}

	return strings.Join(s, " ")
}
