package sorting_test

import (
	"foo/benchmark/sorting"
	"testing"
)

func BenchmarkNewBubble(b *testing.B) {
	data := []int{12, 11, 13, 5, 6, 7}
	bubble := sorting.NewBubble(data)
	for i := 0; i < b.N; i++ {
		bubble.SortASC()
	}
}

func BenchmarkNewMerge(b *testing.B) {
	data := []int{12, 11, 13, 5, 6, 7}
	//bubble := sorting.NewMerge(data)
	for i := 0; i < b.N; i++ {
		sorting.MergeSort(data)
	}
}
