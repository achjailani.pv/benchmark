package main

import "foo/benchmark/strs"

func main() {
	strs.ConcatenateBuffer("val1", "val2")
	strs.ConcatenateJoin("val1", "val2")
}
